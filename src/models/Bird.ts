import ICollidable from '../interfaces/ICollidable';
import IGameEntity from '../interfaces/IGameEntity';
import IDisposable from '../interfaces/IDisposable';
import Ground from './Ground';
import PipesHolder from './PipesHolder';
import Sprite from '../infrastructure/Sprite';

class Bird extends Sprite implements ICollidable, IGameEntity, IDisposable{
    private SPACE_BAR_KEY: number = 32;
    private jumpHeight: number = 30;
    private gravity: number = 1;
    private keyBoardEvent: any;
    
    constructor(){
        super(220, 100, 60, 45, '.bird');
        this.keyBoardEvent = this.control.bind(this);
        document.addEventListener('keyup', this.keyBoardEvent);
    }
    update(): void {
        this.rectangle.yPosition -= this.gravity;
        this.divEntity.style.bottom = `${this.rectangle.yPosition}px`;
        this.divEntity.style.left = `${this.rectangle.xPosition}px`;
    }
    getEntityType(): string {
        return Bird.name;
    }
    checkCollision(other: ICollidable): boolean {
        if(other instanceof Ground){
            if(this.rectangle.yPosition <= 0){
                return true;
            }
        }
        return false;
    }
    collided(other: ICollidable): void {
        if(other instanceof PipesHolder){
            console.log(`Bird: Pipe hits me...`);
        }
        else if(other instanceof Ground){
            console.log(`Bird: Ground hits me...`);
        }
        this.dispose();
    }
    jump(): void{
        if(this.rectangle.yPosition < 500){
            this.rectangle.yPosition += this.jumpHeight;
        }
        this.divEntity.style.bottom = `${this.rectangle.yPosition}px`;
    }
    control(event: KeyboardEvent): void{
        if(event.keyCode === this.SPACE_BAR_KEY){
            this.jump();
        }
    }
    dispose(): void{
        document.removeEventListener('keyup', this.keyBoardEvent);
    }
}

export default Bird;