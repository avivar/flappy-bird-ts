
import IScreen from '../interfaces/IScreen';
import IObserver from '../interfaces/IObserver';
import GameScreen from '../screens/GameScreen';
import WelcomeScreen from '../screens/WelcomeScreen';
import GameOverScreen from '../screens/GameOverScreen';
import LevelTransitionScreen from '../screens/LevelTransitionScreen';
import Stack from '../infrastructure/Stack';
import { GameEvent } from '../infrastructure/enums';

const START_NEW_GAME_KEY = 13;
const RESET_GAME_KEY = 32;
const TOTAL_SECONDS_TRANSITION = 3;

class GameBoard implements IObserver{ 
    private gameScreen: GameScreen
    private levelTransitionScreen: LevelTransitionScreen
    private welcomeScreen: WelcomeScreen
    private gameOverScreen: GameOverScreen
    private screensStack: Stack<IScreen>
    private keyBoardEvent: any
    private isResetGame: boolean

    constructor(isMobile: boolean){
        this.gameScreen = new GameScreen(this, 0, 0);
        this.levelTransitionScreen = new LevelTransitionScreen(TOTAL_SECONDS_TRANSITION);
        this.welcomeScreen = new WelcomeScreen(isMobile);
        this.gameOverScreen = new GameOverScreen();
        this.screensStack = new Stack<IScreen>();
        this.resetScreensStack();
        this.keyBoardEvent = this.startGame.bind(this);
        !isMobile && document.addEventListener('keyup', this.keyBoardEvent);
        this.isResetGame = false;
    }
    buildNewLevel(): void{
        const gameScreen = this.screensStack.peek()! as GameScreen;
        gameScreen.dispose();
        const currentScore = gameScreen.score();
        const currentLevel = gameScreen.level() + 1;
        this.resetGame(currentLevel, currentScore);
    }
    notify(gameEvent: GameEvent): void {
        switch (gameEvent) {
            case GameEvent.LevelUpdated:
                this.buildNewLevel();
                break;
            case GameEvent.GameOver:
                this.handleGameOver();
                break;
        }
    }
    resetScreensStack(): void{
        this.screensStack.push(this.gameOverScreen);
        this.screensStack.push(this.gameScreen);
        this.screensStack.push(this.levelTransitionScreen);
        this.screensStack.push(this.welcomeScreen);
    }
    hideAndShow(): void{
        this.screensStack.peek()!.hide();
        this.screensStack.pop();
        this.screensStack.peek()!.show();
    }
    startNewGame(): void{
        this.hideAndShow();
        setTimeout(this.hideAndShow.bind(this), TOTAL_SECONDS_TRANSITION * 1000);
        document.removeEventListener('keyup', this.keyBoardEvent);
    }
    resetGame(level: number = 1, score: number = 0): void{
        this.isResetGame = false;
        this.screensStack.peek()!.hide();
        if(this.screensStack.peek() instanceof GameScreen){
            this.screensStack.pop();
        }
        this.gameScreen = new GameScreen(this, level, score);
        this.levelTransitionScreen = new LevelTransitionScreen(TOTAL_SECONDS_TRANSITION, level, level * (level * 100));
        this.screensStack.push(this.gameScreen);
        this.screensStack.push(this.levelTransitionScreen);
        this.screensStack.peek()!.show();
        setTimeout(this.hideAndShow.bind(this), TOTAL_SECONDS_TRANSITION * 1000);
        document.removeEventListener('keyup', this.keyBoardEvent);
    }
    startGame(event: KeyboardEvent): void{
        switch (event.keyCode) {
            case START_NEW_GAME_KEY:
                !this.isResetGame && this.startNewGame();
                break;
            case RESET_GAME_KEY:
                this.isResetGame && this.resetGame();
                break;
        }
    }
    start(): void{
        const welcomeScreen: IScreen = this.screensStack.peek()!;
        welcomeScreen.show();
    }
    handleGameOver(): void{
        let currentScreen: IScreen = this.screensStack.peek()!;
        if(currentScreen instanceof GameScreen){
            currentScreen.hide();
            (currentScreen as GameScreen).dispose();
            this.screensStack.pop();
            this.screensStack.peek()!.show();
            this.isResetGame = true;
            document.addEventListener('keyup', this.keyBoardEvent);
        }
    }
}

export default GameBoard;