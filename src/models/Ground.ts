import ICollidable from '../interfaces/ICollidable';
import Bird from './Bird';
import Sprite from '../infrastructure/Sprite';

class Ground extends Sprite implements ICollidable{ 
    constructor(){
        super(500, 150, 0, 580, '.ground-container');
    }
    getEntityType(): string {
        return Ground.name;
    }
    checkCollision(other: ICollidable): boolean {
        return false;
    }
    collided(other: ICollidable): void {
        if(other instanceof Bird){
            console.log(`Ground: Bird hits me...`);
        }
    }
}

export default Ground;