import Sprite from '../infrastructure/Sprite';
import IGameEntity from '../interfaces/IGameEntity';
import IDisposable from '../interfaces/IDisposable';

class Pipe extends Sprite implements IGameEntity, IDisposable{
    public static TOP_PIPE_GAP:number = 430;
    public static PIPE_WIDTH:number = 60;
    public static VELOCITY: number = 2;
    private isTopPipe: boolean;

    constructor(isTopPipe: boolean){
        let pipeBottomPosition = (Math.random() * 35);
        super(500,
            isTopPipe ? pipeBottomPosition + Pipe.TOP_PIPE_GAP : pipeBottomPosition, Pipe.PIPE_WIDTH, 300, '');
        this.divEntity.classList.add(isTopPipe ? 'top-pipe' : 'pipe');
        this.divEntity.style.left = `${this.rectangle.xPosition}px`;
        this.divEntity.style.bottom = `${this.rectangle.yPosition}px`;
        this.isTopPipe = isTopPipe;
    }
    update(): void {
        this.rectangle.xPosition -= Pipe.VELOCITY;
        this.divEntity.style.left = `${this.rectangle.xPosition}px`;
    }
    getIsTopPipe(): boolean{
        return this.isTopPipe;
    }
    dispose(): void{
        this.divEntity.remove();
    }
}

export default Pipe;