import Pipe from './Pipe';
import Bird from './Bird';
import ICollidable from '../interfaces/ICollidable';
import IGameEntity from '../interfaces/IGameEntity';
import IDisposable from '../interfaces/IDisposable';
import IObserver from '../interfaces/IObserver';
import { GameEvent } from '../infrastructure/enums';

class PipesHolder implements ICollidable, IGameEntity, IDisposable{
    private pipesArray: Pipe[];
    public gameScreen: IObserver;

    constructor(gameScreen: IObserver){
        this.pipesArray = new Array();
        this.gameScreen = gameScreen;
        Pipe.VELOCITY = 2;
    }
    update(): void {
        for (let i = 0; i < this.pipesArray.length; i++) {
            const singlePipe = this.pipesArray[i];
            singlePipe.update();
            if(singlePipe.rectangle.xPosition <= -60){
                singlePipe.dispose();
            }
        }
    }
    getEntityType(): string {
        return PipesHolder.name;
    }
    checkCollision(other: ICollidable): boolean {
        if(other instanceof Bird){
            const bird = other as Bird;
            for (let i = 0; i < this.pipesArray.length; i++) {
                const singlePipe = this.pipesArray[i];
                if((singlePipe.rectangle.xPosition > (bird.rectangle.xPosition - 20)
                    && singlePipe.rectangle.xPosition  < (bird.rectangle.xPosition + bird.rectangle.width)))
                    {
                        let isTopPipe = singlePipe.getIsTopPipe();
                        if((!isTopPipe && bird.rectangle.yPosition < singlePipe.rectangle.yPosition + 153)
                        || (isTopPipe && bird.rectangle.yPosition > singlePipe.rectangle.yPosition - 200)){
                            return true;
                        }
                    }
                if(singlePipe.rectangle.xPosition === (bird.rectangle.xPosition - 20)){
                    this.gameScreen.notify(GameEvent.ScoreUpdated);
                }
            }
        }
        return false;
    }
    collided(other: ICollidable): void {
        if(other instanceof Bird){
            console.log(`Pipe: Bird hits me...`);
        }
    }
    addPipe(pipe: Pipe): void{
        this.pipesArray.push(pipe);
    }
    generatePipe(isTopPipe: boolean): Pipe{
        let newPipe = new Pipe(isTopPipe);
        this.addPipe(newPipe);
        return newPipe;
    }
    dispose(): void{
        for (let i = 0; i < this.pipesArray.length; i++) {
            this.pipesArray[i].dispose();
        }
        this.pipesArray = new Array();
    }
}

export default PipesHolder;