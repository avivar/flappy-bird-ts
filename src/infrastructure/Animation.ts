
abstract class Animation{ 
    private _animationClassName: string;
    private _isInfinite: boolean;
    private _entity: HTMLDivElement;
    private _animationTimeSeconds: number;
    private _isActive: boolean;
    private _isAnimationsEnd: boolean;
    
    constructor(animationClassName: string, isInfinite: boolean, entity: HTMLDivElement, animationTimeSeconds: number){
        this._entity = entity;
        this._isInfinite = isInfinite;
        this._animationClassName = animationClassName;
        this._animationTimeSeconds = animationTimeSeconds;
        this._isActive = false;
        this._isAnimationsEnd = false;
    }
    endAnimation(): void{
        if(this._isInfinite){
            this.doFrame();
        }
        else{
            this._isActive = false;
            this._isAnimationsEnd = true;
        }
    }
    doFrame(): void{
        this._isActive = true;
        this._entity.classList.add(this._animationClassName);
        setTimeout(this.endAnimation.bind(this), this._animationTimeSeconds * 1000);
    }
    resetAnimation(): void{
        this._entity.classList.remove(this._animationClassName);
        this._isActive = false;
        this._isAnimationsEnd = false;
        this.doFrame();
    }
    getIsAnimationActive(): boolean{
        return this._isActive;
    }
    getIsAnimationEnds(): boolean{
        return this._isAnimationsEnd;
    }
}

export default Animation;
