import Rectangle from './Rectangle';
import IDivEntity from '../interfaces/IDivEntity';

abstract class Sprite implements IDivEntity{
    public divEntity: HTMLDivElement;
    public rectangle: Rectangle;

    constructor(xPosition: number, yPosition: number, width: number, height: number, divClassName: string){
        this.rectangle = new Rectangle(xPosition, yPosition, width, height);
        this.divEntity = divClassName ? document.querySelector(divClassName) as HTMLDivElement : document.createElement('div');
    }
}

export default Sprite;