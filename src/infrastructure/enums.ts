export enum GameEvent{
    ScoreUpdated,
    LevelUpdated,
    GameOver
}

