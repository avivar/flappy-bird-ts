class Rectangle{
    public xPosition: number;
    public yPosition: number;
    public width: number;
    public height: number;

    constructor(xPosition: number, yPosition: number, width: number, height: number){
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.width = width;
        this.height = height;
    }
}

export default Rectangle;