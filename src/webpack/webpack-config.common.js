const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

const babelRule = {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
        loader: 'babel-loader'
    }
}

const fileLoaderRule = {
    test: /\.(png|jpe?g|gif)$/i,
    use: [
      {
        loader: 'file-loader',
        options: {
          name: 'media/[name].[ext]'
        }
      },
    ],
}

const miniCssRule = {
    test: /\.css$/i,
    use: [
      {
        loader: MiniCssExtractPlugin.loader,
        options: {
          esModule: true,
        },
      },
      'css-loader',
    ],
}

const typeScriptRule = {
  test: /\.ts$/,
  exclude: /node_modules/,
  use: {
      loader: 'ts-loader',
      options: {
        transpileOnly: true,
        happyPackMode: true,
        configFile: path.resolve(__dirname, 'tsconfig.json'),
      },
  }
}

const jsxRule = {
  test: /\.jsx?$/,
  loader: 'babel-loader',
  exclude: /node_modules/,
  query: {
    cacheDirectory: true,
    presets: ['react', 'es2015']
  }
}

module.exports.babelRule = babelRule;
module.exports.miniCssRule = miniCssRule;
module.exports.fileLoaderRule = fileLoaderRule;
module.exports.MiniCssExtractPlugin = MiniCssExtractPlugin;
module.exports.typeScriptRule = typeScriptRule;
module.exports.jsxRule = jsxRule;
module.exports.appName = "FlappyBird";