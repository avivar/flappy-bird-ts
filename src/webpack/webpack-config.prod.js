const webpack = require('webpack');
const format = require('react-dev-utils/formatWebpackMessages');
const { join } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { babelRule, fileLoaderRule, miniCssRule, MiniCssExtractPlugin, appName, typeScriptRule, jsxRule } = require('./webpack-config.common');
const PRODUCTION = 'production';
const { EOL } = require('os');
const chalk = require('chalk');

const environmentVariablePlugin = new webpack.DefinePlugin({
    'ENVIRONMENT': JSON.stringify(PRODUCTION)
})

const htmlPlugin = new HtmlWebpackPlugin({
    template: join(process.cwd(), 'src', 'index.html'),
    filename: 'index.html'
})

const miniCssExtractPlugin = new MiniCssExtractPlugin({
    filename: '[name].[hash].css',
    chunkFilename: '[id].[hash].css',
});

const configProd = {
    name: appName,
    mode: PRODUCTION,
    entry: join(process.cwd(), 'src', 'index.ts'),
    output: {
        path: join(process.cwd(), 'build'),
        filename: 'js/bundle.prod.js'
    },
    resolve: {
        extensions: [".ts", ".jsx"]
    },
    module: {
        rules: [ babelRule, miniCssRule, fileLoaderRule, typeScriptRule, jsxRule ]
    },
    plugins: [
        environmentVariablePlugin,
        htmlPlugin,
        miniCssExtractPlugin,
    ],
    optimization: {
        minimize: true
    }
}

const writeLogs = (errors, warnings) => {
    let buildMessage = '';
    if(errors && errors.length){
        console.log(chalk.bgRed('***ERRORS***'));
        errors.forEach(err => console.log(err));
        buildMessage = chalk.redBright('Build failed!');
    }
    else if(warnings && warnings.length){
        console.log(chalk.black.bgYellowBright('***WARNINGS***'));
        warnings.forEach(warn => console.log(warn));
        buildMessage = chalk.yellowBright('Build succeeded with warninngs!');
    }
    else{
        buildMessage = chalk.greenBright('Build succeeded!');
    }
    console.log(`${EOL}${buildMessage}`);
}

const compiler = webpack(configProd);
compiler.run((err, stats) => {
    if(err){
        throw err;
    }
    const { errors, warnings } = format(stats.toJson({}, true));
    writeLogs(errors, warnings);
})