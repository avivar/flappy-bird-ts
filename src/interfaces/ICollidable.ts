interface ICollidable{
    checkCollision(other: ICollidable): boolean;
    collided(other: ICollidable): void
    getEntityType(): string
}

export default ICollidable;