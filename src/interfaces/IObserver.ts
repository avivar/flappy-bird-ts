import { GameEvent } from '../infrastructure/enums';

interface IObserver{
    notify(gameEvent: GameEvent): void
}

export default IObserver;