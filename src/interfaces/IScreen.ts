interface IScreen{
    show(): void
    hide(): void
}

export default IScreen;