import Screen from '../screens/Screen';

class GameOverScreen extends Screen{
    constructor(){
        super('.game-over-screen-container');
    }
    hide(): void {
        this.removeVisibleClass();
    }
    show(): void {
        this.addVisibleClass();
    }
}

export default GameOverScreen;
