import IDivEntity from '../interfaces/IDivEntity';
import Screen from '../screens/Screen';

class WelcomeScreen extends Screen{ 
    private mobileMessageContainer: HTMLDivElement
    private instructionsElement: HTMLDivElement

    constructor(isMobile: boolean){
        super('.welcome-screen-container');
        this.mobileMessageContainer = document.querySelector('.welcome-screen-container .mobile-message') as HTMLDivElement;
        this.instructionsElement = document.querySelector('.welcome-screen-container .welcome-instructions') as HTMLDivElement;
        this.instructionsElement.classList.add('screen-visible');
        if(isMobile){
            this.divEntity.classList.add('mobile-overlay');
            this.mobileMessageContainer.classList.add('screen-visible');
            this.instructionsElement.classList.remove('screen-visible');
        }
    }
    hide(): void {
        this.removeVisibleClass();
    }
    show(): void {
        this.addVisibleClass();
    }
}

export default WelcomeScreen;
