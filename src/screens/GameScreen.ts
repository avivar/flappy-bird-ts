import Bird from '../models/Bird';
import PipesHolder from '../models/PipesHolder';
import Pipe from '../models/Pipe';
import ICollidable from '../interfaces/ICollidable';
import IDisposable from '../interfaces/IDisposable';
import Ground from '../models/Ground';
import IDivEntity from '../interfaces/IDivEntity';
import IObserver from '../interfaces/IObserver';
import Screen from '../screens/Screen';
import { GameEvent } from '../infrastructure/enums';

class GameScreen extends Screen implements IDisposable, IObserver{ 
    private bird: Bird
    private ground: Ground
    private scoreBoard: HTMLDivElement
    private scoreBoardContainer: HTMLDivElement
    private _isGameOver: boolean
    private gameTimerId: any
    private PipesHolder: PipesHolder
    private gameFrameInterval: number
    private generatePipeInterval: number
    private _score: number
    private _level: number
    private collidableComponents: ICollidable[];
    private gameBoard: IObserver;
    private isVisible: boolean;

    constructor(gameBoard: IObserver, level: number, score: number){
        super('.flappy-bird-container')
        this.gameBoard = gameBoard;
        this.bird = new Bird();
        this.ground = new Ground();
        this.scoreBoard = document.querySelector('.game-container .score-container .score-value') as HTMLDivElement;
        this.scoreBoardContainer = document.querySelector('.game-container .score-container') as HTMLDivElement;
        this._isGameOver = false;
        this.gameTimerId = null;
        this.PipesHolder = new PipesHolder(this);
        this.gameFrameInterval = 20;
        this.generatePipeInterval = level > 1 ? 1500 : 2000;
        this._score = score || 0;
        this._level = level || 1;
        this.collidableComponents = new Array();
        this.addObjectToMonitor(this.bird);
        this.addObjectToMonitor(this.PipesHolder);
        this.addObjectToMonitor(this.ground);
        this.isVisible = false;
    }
    notify(gameEvent: GameEvent): void {
        switch (gameEvent) {
            case GameEvent.ScoreUpdated:
                this.notifyScoreUpdated();
                break;
        }
    }
    hide(): void {
        this.removeVisibleClass();
        this.isVisible = false;
    }
    show(): void {
        this.addVisibleClass();
        this.isVisible = true;
        this.scoreBoardContainer.classList.add('screen-visible');
        this.start();
    }
    start(): void{
        this.scoreBoard.innerHTML = `${this._score}`;
        this.gameTimerId = setInterval(this.update.bind(this), this.gameFrameInterval);
        setTimeout(this.generatePipe.bind(this), this.generatePipeInterval - 1000);
    }
    addObjectToMonitor(entity: ICollidable): void{
        this.collidableComponents.push(entity);
    }
    notifyScoreUpdated(): void{
        this._score += 10;
        if(this._score >= (100 * this._level) * this._level){
            this.gameBoard.notify(GameEvent.LevelUpdated);
            const pipeVelocity = ((this._level + 1) * 2);
            if((Pipe.PIPE_WIDTH % pipeVelocity) === 0){
                Pipe.VELOCITY = pipeVelocity;
            }
        }
    }
    checkIntersection(): void{
        for (let i = 0; i < this.collidableComponents.length; i++) {
            const sourceEntity = this.collidableComponents[i];
            for (let j = 0; j < this.collidableComponents.length; j++) {
                const targetEntity = this.collidableComponents[j];
                if(sourceEntity.getEntityType() === targetEntity.getEntityType()){
                    continue;
                }
                if(sourceEntity.checkCollision(targetEntity)){
                    sourceEntity.collided(targetEntity);
                    targetEntity.collided(sourceEntity);
                    this.gameOver();
                    break;
                }
            }
        }
    }
    update(): void{
        if(!this._isGameOver && this.isVisible){
            this.bird.update();
            this.PipesHolder.update();
            this.checkIntersection();
            this.updateBoard();
        }
    }
    updateBoard(): void{
        this.scoreBoard.innerHTML = `${this._score}`;
    }
    addEntity(entity: HTMLDivElement): void{
        this.divEntity.appendChild(entity);
    }
    generatePipe(): void{
        if(!this._isGameOver && this.isVisible ){//&& this._score < (100 * this._level * this._level) - 20
            this.addEntity(this.PipesHolder.generatePipe(false).divEntity);
            this.addEntity(this.PipesHolder.generatePipe(true).divEntity);
            setTimeout(this.generatePipe.bind(this), this.generatePipeInterval - (Math.random() * 1000));
        }
    }
    gameOver(): void{
        console.log('GAME OVER!');
        this._isGameOver = true;
        clearInterval(this.gameTimerId);
        this.gameBoard.notify(GameEvent.GameOver);
    }
    isGameOver(): boolean{
        return this._isGameOver;
    }
    level(): number{
        return this._level;
    }
    score(): number{
        return this._score;
    }
    dispose(): void{
        this.PipesHolder.dispose();
    }
}

export default GameScreen;
