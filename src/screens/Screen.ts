import IDivEntity from '../interfaces/IDivEntity';
import IScreen from '../interfaces/IScreen';

abstract class Screen implements IDivEntity, IScreen{ 
    public divEntity: HTMLDivElement;

    constructor(className: string){
        this.divEntity = document.querySelector(className) as HTMLDivElement;
    }
    addVisibleClass(): void{
        this.divEntity.classList.add('screen-visible');
    }
    removeVisibleClass(): void{
        this.divEntity.classList.remove('screen-visible');
    }
    abstract show(): void;
    abstract hide(): void;
}

export default Screen;
