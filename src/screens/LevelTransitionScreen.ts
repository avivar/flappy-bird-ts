import Screen from '../screens/Screen';

class LevelTransitionScreen extends Screen{ 
    public countDownElement: HTMLDivElement;
    public levelElement: HTMLDivElement;
    public instructionsElement: HTMLDivElement;
    private screenCountDownTimerId: any
    private totalSeconds: number

    constructor(totalSeconds: number, level: number = 1, pointToPassLevel: number = 100){
        super('.level-transition-screen-container');
        this.countDownElement = document.querySelector('.level-transition-screen-container .show-count-down') as HTMLDivElement;
        this.levelElement = document.querySelector('.level-transition-screen-container .show-level') as HTMLDivElement;
        this.levelElement.innerHTML = `Level ${level}`;
        this.instructionsElement = document.querySelector('.level-transition-screen-container .level-instructions') as HTMLDivElement;
        this.instructionsElement.innerHTML = `Collect ${pointToPassLevel} points`;
        this.totalSeconds = totalSeconds;
        this.countDownElement.innerHTML = `${this.totalSeconds}`;
        this.screenCountDownTimerId = null;
    }
    hide(): void {
        this.removeVisibleClass();
        clearInterval(this.screenCountDownTimerId);
    }
    show(): void {
        this.addVisibleClass();
        this.screenCountDownTimerId = setInterval(this.levelCountDown.bind(this), 1000);
    }
    levelCountDown(): void{
        if(this.totalSeconds > 1){
            this.countDownElement.innerHTML = `${--this.totalSeconds}`;
        }
    }
}

export default LevelTransitionScreen;
